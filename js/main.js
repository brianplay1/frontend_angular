var app = angular.module("dependentDropdown", ['ui.router', 'ui.select']);

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
   $stateProvider
   .state('home', {
      url: '/home',
      templateUrl: 'templates/main.html',
      controller: 'homeController'
   });
   $urlRouterProvider.otherwise('home');
}]);

app.controller("homeController", ['$scope', '$filter', function($scope, $filter){

   $scope.showInitial = false;

   $scope.details = {
      Company:'',
      Trainer: '',
      Trainee: ''
   };

   $scope.companies = [
      {"id": 1, "company": "Taj"},
      {"id": 2, "company": "Fortis"},
      {"id": 3, "company": "Oberoi"}
   ];

   var traineeList = [
      {"id": 1, "trainee": "Ankit", "companyId": 1},
      {"id": 2, "trainee": "Anurag", "companyId": 2},
      {"id": 3, "trainee": "Ajay", "companyId": 3},
      {"id": 4, "trainee": "Bimol", "companyId": 3},
      {"id": 5, "trainee": "Sanjay", "companyId": 2},
      {"id": 6, "trainee": "Ramesh", "companyId": 1}
   ];

   var trainerList = [
      {"id": 1, "trainer": "Nitin", "companyId": 1},
      {"id": 2, "trainer": "Puneet", "companyId": 1},
      {"id": 3, "trainer": "Sumit", "companyId": 2},
      {"id": 4, "trainer": "Karan", "companyId": 2},
      {"id": 5, "trainer": "Pranay", "companyId": 3}
   ];

   $scope.criteria = ['Practical', 'Written test', 'Viva', 'Roleplay'];

   $scope.$watch('details.Company', function(NewValue, OldValue) {
        console.log(OldValue, NewValue);
        $scope.getTraineeByCompany(NewValue.id)
    }, true);

    $scope.$watch("details.Trainee", function(NewValue, OldValue){
      console.log(OldValue);
      console.log(typeof NewValue);
      console.log(NewValue);
      if(NewValue !== '')
         $scope.getAllData();
   }, true);

   $scope.getTraineeByCompany = function(id){
      $scope.showInitial = false;
      var trainee = $filter('filter')(traineeList, {companyId: id});
      $scope.newTraineeList =  trainee;
   };

   $scope.getAllData = function(){
      console.log('getAllData');
      console.log($scope.details);
      $scope.newData = {
         Company: '',
         // Trainer: '',
         Trainee: ''
      };
      $scope.newData.Company = $filter("filter")($scope.companies, {id: $scope.details.Company.id});

      if($scope.details.Trainee){
         $scope.newData.Trainee = $filter("filter")(traineeList, {id: $scope.details.Trainee.id});
      }
      else{
         $scope.newData.Trainee = $filter('filter')(traineeList, {companyId: $scope.details.Company.id});
      }

      $scope.showInitial = true;
   };

   // --------------------------------------------------------------------
   $scope.criteria_index = 0;

   $scope.passFail = function(criteria, id, status){
      console.log(criteria, id);
      if ($scope.criteria_index >= $scope.criteria.length - 1) {
         $scope.criteria_index = 0;
      } else {
         $scope.criteria_index++;
      }
      var trainee = $filter('filter')($scope.newTraineeList, {id: id});
      console.log(trainee);
      trainee[0][criteria] = status;
   };

   $scope.next = function(criteria, id){
      console.log(criteria, id);
      if ($scope.criteria_index >= $scope.criteria.length - 1) {
         $scope.criteria_index = 0;
      } else {
         $scope.criteria_index++;
      };
   };

   $scope.previous = function(criteria, id){
      console.log(criteria, id);
      if ($scope.criteria_index <= 0) {
         $scope.criteria_index = $scope.criteria.length - 1;
      } else {
         $scope.criteria_index--;
      };
   }


   // --------------------------------------------------------------------
}]);
